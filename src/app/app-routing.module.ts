import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ArticleFormComponent} from './article-form/article-form.component';

const routes: Routes = [
  { path: '**', redirectTo: '/home' },
  { path: 'home', component: ArticleFormComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
