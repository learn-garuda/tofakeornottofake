export class SearchObject {
  constructor(
    public title: string,
    public description: string,
    public link: string,
    public imageUrl: string) {
  }
}
