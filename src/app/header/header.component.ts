import { Component, OnInit } from '@angular/core';
import {faNewspaper} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {

  public faNewspaper = faNewspaper;

  constructor() { }

  ngOnInit(): void {
  }

}
