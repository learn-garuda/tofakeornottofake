import { Component, OnInit } from '@angular/core';
import {faBook, faUser} from '@fortawesome/free-solid-svg-icons';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {SearchObject} from '../models/SearchObject';

@Component({
  selector: 'app-article-form',
  templateUrl: './article-form.component.html',
  styleUrls: ['./article-form.component.sass']
})
export class ArticleFormComponent implements OnInit {

  constructor(private httpClient: HttpClient) { }

  public searchObjects: Array<SearchObject> = [];
  public faUser = faUser;
  public faBook = faBook;
  text = '';
  score = '-- %';
  colors = {
    normal: '#b5b5b5',
    red: '#c53419',
    orange: '#d57b1a',
    yellow: '#f0ca0d',
    light_green: '#97ba38',
    dark_green: '#2d7e43'
  };
  currentColor = this.colors.normal;

  articleForm = new FormGroup({
    title: new FormControl('', Validators.required),
    author: new FormControl('', Validators.required),
    article: new FormControl('', Validators.required),
    isFake: new FormControl(false, Validators.required)
  });

  header = new HttpHeaders({
    'Ocp-Apim-Subscription-Key': '27dcb8fa44b24768af52a4daa07f8a55',
    Accept: 'application/json'
  });

  ngOnInit(): void {
  }

  // Take the score and multiply it by 100 and take the correct color to display
  changeColor(score: number): void {
    score = (1 - score) * 100;
    this.score = score.toString() + ' %';
    if (score  >= 0 && score < 20) {
        this.currentColor = this.colors.red;
      } else if (score >= 20 && score < 40) {
        this.currentColor = this.colors.orange;
      } else if (score >= 40 && score < 60) {
        this.currentColor = this.colors.yellow;
      } else if (score >= 60 && score < 80) {
        this.currentColor = this.colors.light_green;
      } else if (score >= 80 && score <= 100) {
        this.currentColor = this.colors.dark_green;
      } else {
        this.score = 'ERROR';
        this.currentColor = '#ff3860';
      }
  }
  // tslint:disable-next-line:ban-types
  searchRequest(): Promise<Object> {
      return new Promise((resolve, reject) => {
        // tslint:disable-next-line:max-line-length
        this.httpClient.get('https://api.bing.microsoft.com/v7.0/news/search?q=' + encodeURIComponent(this.articleForm.get('title')?.value), {
          headers: this.header,
          observe: 'response'
        }).subscribe({
          next: data => resolve(data),
          error: data => reject(data)
        });
      });
  }

  // tslint:disable-next-line:ban-types
  private fakeRequest(body: Object): Promise<Object> {
    return new Promise((resolve, reject) => {
      this.httpClient.post(
        'https://ussouthcentral.services.azureml.net/workspaces/6f15be2459da4f33955f593a18873d24/services/6db33188aa424d7385b88b18ab4bcb40/execute?api-version=2.0&details=true',
        body,
        {
          headers: new HttpHeaders({
            Authorization: 'Bearer AvjgE7U4BqPLwZDvIiq6xf2E6EMCjxwrOKhezUNPvCGXwy6pqwz6Y3rwTvqKX/z3XHjQ7/5ObMtmra4qvS0jwA==',
            'Content-Type': 'application/json',
            Accept: 'application/json'
          }),
          observe: 'response'
        }).subscribe({
        next: data => resolve(data),
        error: data => reject(data)
      });
    });
  }

  private fakeModelML(): void {
    const body = {
      Inputs: {
        input1: {
          ColumnNames: [
            'id',
            'title',
            'author',
            'text',
            'label'
          ],
          Values: [
            [
              '0',
              this.articleForm.get('title')?.value,
              this.articleForm.get('author')?.value,
              this.articleForm.get('article')?.value,
              this.articleForm.get('isFake')?.value ? 1 : 0
            ]
          ]
        }
      },
      GlobalParameters: {}
    };

    this.fakeRequest(body)
      .then(response => {
        // tslint:disable-next-line:radix
        // @ts-ignore
        const proba = parseFloat(response.body.Results.output1.value.Values[0][5]);
        this.changeColor(proba);
      }).catch(console.log);
  }

  // Call IA and use the response
  submit(): void {
    this.fakeModelML();
    this.bingNewsSearch();
  }
  // perform a search given query, options string, and API key
  bingNewsSearch(): void {
    this.searchRequest()
      .then(response => {
        this.searchObjects = [];
        // @ts-ignore
        // tslint:disable-next-line:no-shadowed-variable
        response.body.value.forEach(news => {
          this.searchObjects.push(new SearchObject(news.name, news.description, news.url, news.image.thumbnail.contentUrl));
        });
      })
      .catch(error => console.log(error));
  }


  // To remove all values of the form
  clear(): void {
    this.articleForm.patchValue({
      title: '',
      author: '',
      article: '',
      isFake: false
    });

    this.searchObjects = [];
  }
}
