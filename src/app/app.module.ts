import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ArticleFormComponent } from './article-form/article-form.component';
import {ReactiveFormsModule} from '@angular/forms';
import { SearchObjectComponent } from './search-object/search-object.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ArticleFormComponent,
    SearchObjectComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FontAwesomeModule,
        ReactiveFormsModule,
        HttpClientModule,
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
