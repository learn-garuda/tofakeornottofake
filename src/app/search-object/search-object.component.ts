import {Component, Input, OnInit} from '@angular/core';
import {SearchObject} from '../models/SearchObject';

@Component({
  selector: 'app-search-object',
  templateUrl: './search-object.component.html',
  styleUrls: ['./search-object.component.sass']
})
export class SearchObjectComponent implements OnInit {

  @Input() searchObject!: SearchObject;

  constructor() { }

  ngOnInit(): void { }

}
